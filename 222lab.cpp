#include <iostream>
#include <iomanip>
using namespace std;

void celciusToFahrenheit(double c)
{
	double f;
	f = c*double(9.0/5.0)+ 32;
	cout << c << " celcius = " << setprecision(5)<<f << " fahrenheit" << endl;
}
void fahrenheitToCelcius(double f) {
	double c;
	c = (f - 32)*(double)(5.0/9.0);
	cout << f << " fahrenheit = " << setprecision(5) << c << " celcius" << endl;
}
void circumference(double radius) {
    while(radius<1)
    {
        cout<<"Radius must be most than 1!"<<endl;
        cout<<"Reenter radius:";
        cin>>radius;
    }
	double a= 2 * double(22.0 / 7.0) * radius;
	cout << "Circumference of circle = " << setprecision(5)  << a << endl;
}
void area(double radius) {
    while(radius<1)
    {
        cout<<"Radius must be most than 1!"<<endl;
        cout<<"Reenter radius:";
        cin>>radius;
    }
	double a= double(22.0 / 7.0) * radius*radius;
	cout << "Area of circle = " << setprecision(5) << a << endl;
}
int main()
{
	int choice;
	double input;
	bool notquit = true;
	do{
		cout << "1.	Convert Celsius to Fahrenheit" << endl;
		cout << "2.	Convert Fahrenheit to Celsius" << endl;
		cout << "3.	Calculate Circumference of a circle" << endl;
		cout << "4.	Calculate Area of a circle" << endl;
		cout << "5.	Quit program" << endl;
		cout << "Enter a choice: ";
		cin >> choice;

		switch (choice) {
		case 1:
			cout << "Enter Celsius: ";
			cin >> input;
			celciusToFahrenheit(input);
			break;
		case 2:
			cout << "Enter Fahrenheit: ";
			cin >> input;
			fahrenheitToCelcius(input);
			break;
		case 3:
			cout << "Enter radius (m): ";
			cin >> input;
			circumference(input);
			break;
		case 4:
			cout << "Enter radius (m): ";
			cin >> input;
			area(input);
			break;
		case 5:
			notquit = false;
			break;

		}
	}while (notquit);
	return 0;
}
